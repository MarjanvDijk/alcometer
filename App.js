import React, { useState } from "react";
import {
  Text,
  TextInput,
  ScrollView,
  View,
  Pressable,
  Alert,
} from "react-native";
import Styles from "./Style/Styles";
import { Picker } from "@react-native-picker/picker";
import RadioButton from "./Component/RadioButton";

/**
 * @author Marjan van Dijk
 *
 * Function compontents to show the alcometer app.
 * It is based and tested for an android device since I dont have acces to an IOS device.
 */

export default function App() {
  /**
   * Creating constants that will be used in the app
   */
  const [weight, setWeight] = useState("");
  const [bottles, setBottles] = useState(0);
  const [time, setTime] = useState(1);
  const [gender, setGender] = useState(0);
  const [alcoholLevel, setAlcoholLevel] = useState(0);

  const bottlesArray = Array(31)
    .fill("")
    .map((_, i) => ({ label: `${i} bottles`, value: `${i}` }));

  const hours = Array(12)
    .fill("")
    .map((_, i) => ({ label: `${i + 1} hours`, value: `${i + 1}` }));

  const genders = [
    { label: "Male", value: 1 },
    { label: "Female", value: 2 },
  ];

  /**
   * Functin getAlcoholLevel is a function that will calculate the blood alcohol level.
   * It takes the values from the state variables and checks if the weight or the gender is not selected, if so an error will pop up.
   * If all value's are filled in, it will calculate the alcohol level and change the state variable to the new alcohol level.
   */
  function getAlcoholLevel() {
    if (weight === "") {
      Alert.alert("Error while calculating", "Weight has not been filled in", [
        { text: "OK" },
      ]);
    } else if (gender === 0) {
      Alert.alert("Error while calculating", "Gender has not been selected", [
        { text: "OK" },
      ]);
    } else {
      const litres = bottles * 0.33;
      const grams = litres * 8 * 4.5;
      const burning = weight / 10;
      const gramsLeft = grams - burning * time;
      let result =
        gender === 1
          ? (gramsLeft / (weight * 0.7)).toFixed(2)
          : (gramsLeft / (weight * 0.6)).toFixed(2);
      if (result < 0) result = 0;
      setAlcoholLevel(result);
    }
  }

  /**
   * View that is displayed on the screen.
   * Different sections for each 'block'. Each block contains a label and a way to fill in data, either with a text input, dropdown picker or radio buttons.
   * Based on the alcohol level the text coloir of the result is changed.
   */
  return (
    <ScrollView style={Styles.container}>
      <View style={Styles.header}>
        <Text style={Styles.headerTitle}>Alcometer</Text>
      </View>

      <View style={Styles.block}>
        <Text style={Styles.label}>Weight</Text>
        <TextInput
          style={Styles.textInput}
          value={weight}
          keyboardType="decimal-pad"
          onChangeText={(weightInput) =>
            setWeight(weightInput.replace(",", "."))
          }
        />
      </View>
      <View style={Styles.block}>
        <Text style={Styles.label}>Bottles</Text>
        <Picker
          style={Styles.picker}
          selectedValue={bottles}
          onValueChange={(bottleInput) => setBottles(bottleInput)}
        >
          {bottlesArray.map((bottles) => (
            <Picker.Item
              label={bottles.label}
              value={bottles.value}
              key={bottles.label}
            />
          ))}
        </Picker>
      </View>
      <View style={Styles.block}>
        <Text style={Styles.label}>Time</Text>
        <Picker
          style={Styles.picker}
          selectedValue={time}
          onValueChange={(timeInput) => setTime(timeInput)}
        >
          {hours.map((time) => (
            <Picker.Item
              label={time.label}
              value={time.value}
              key={time.label}
            />
          ))}
        </Picker>
      </View>
      <View style={Styles.block}>
        <Text style={Styles.label}>Gender</Text>
        <RadioButton
          options={genders}
          onPress={(value) => {
            setGender(value);
          }}
        />
      </View>
      <View style={Styles.block}>
        <Text
          style={[
            Styles.resultText,
            alcoholLevel <= 0.4
              ? Styles.greenText
              : alcoholLevel > 2
              ? Styles.redText
              : Styles.orangeText,
          ]}
        >
          {alcoholLevel}
        </Text>
      </View>
      <View style={Styles.block}>
        <Pressable onPress={getAlcoholLevel} style={Styles.button}>
          <Text style={Styles.buttonText}>Calculate blood alcohol level</Text>
        </Pressable>
      </View>
    </ScrollView>
  );
}
