import { StyleSheet } from "react-native";
import Constants from "expo-constants";

/**
 * @author Marjan van Dijk
 * StyleSheet that is used for the app.js and radiobutton.js
 */
export default StyleSheet.create({
  header: {
    marginTop: Constants.statusBarHeight,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  headerTitle: {
    paddingVertical: 20,
    fontSize: 32,
    color: "#349beb",
    fontWeight: "bold",
  },
  container: {
    margin: 5,
  },
  block: {
    padding: 15,
  },
  label: {
    fontWeight: "bold",
    fontSize: 18,
  },
  textInput: {
    paddingHorizontal: 5,
    borderBottomColor: "darkgray",
    borderBottomWidth: 1,
  },
  picker: {
    marginBottom: 10,
  },
  radioButtonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    width: "100%",
    marginBottom: 10,
    marginBottom: 30,
  },
  circle: {
    height: 24,
    width: 24,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: "#000",
    alignItems: "center",
    justifyContent: "center",
  },
  checkedCirlce: {
    width: 14,
    height: 14,
    borderRadius: 10,
    backgroundColor: "#000",
  },
  resultText: {
    fontSize: 32,
    alignSelf: "center",
  },
  button: {
    backgroundColor: "#349beb",
    alignItems: "center",
    borderRadius: 8,
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    marginVertical: 8,
  },
  redText: {
    color: "#f54242",
  },
  greenText: {
    color: "#10a802",
  },
  orangeText: {
    color: "#f58200",
  },
});
