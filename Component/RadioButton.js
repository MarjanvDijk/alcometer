import React, { useState } from "react";
import { Text, View, Pressable } from "react-native";
import Styles from "../Style/Styles";

/**
 * @author Marjan van Dijk
 *
 * Component for the self made radio buttons to select the gender.
 */
export default function RadioButton({ options, onPress }) {
  const [value, setValue] = useState(null);

  function handlePress(selected) {
    setValue(selected);
    onPress(selected);
  }

  return (
    <>
      {options.map((item) => (
        <View key={item.value} style={Styles.radioButtonContainer}>
          <Text>{item.label}</Text>
          <Pressable
            style={Styles.circle}
            onPress={() => handlePress(item.value)}
          >
            {value === item.value && <View style={Styles.checkedCirlce} />}
          </Pressable>
        </View>
      ))}
    </>
  );
}
